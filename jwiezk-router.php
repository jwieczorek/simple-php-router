<?php
namespace JWIEZK;
###################################
## 
##	File: jwiezk-router.php
##	Author: Joshua Wieczorek (http://www.joshuawieczorek.com)
##
##          -- File Description -- 
##	This class file is a simple php router for any application.
## 
###################################

class Router
{
    ## Private: Query param to get
    private $_query_param       = 'uri';
    ## Private: Query delimiter to explode params by
    private $_query_delimiter   = '/';
    ## Private: URI from $_GET method 
    private $_getUri            = '';
    ## Private: URI array
    private $_getUriArray       = '';
    ## Private: URI array set by user
    private $_uri               = array();
    ## Private funcs array (optional)
    private $_funcs             = array();
    ## Private: namespace (optional)
    private $_namespace         = '';
    ## Private: Home page
    private $_homeURI           = 'home';
    ## Public: 404 Error
    public $error_404           = '';
    
    ###################################
    ## Router->set_query_param( $param )
    ###################################    
    /**         -- Params --
     *  $param : (string) query parameter - default is "uri"
     *          -- Description --
     *  This method allows the setting of the query parameter that is retrieved.
     */
    public function set_query_param( $param='' )
    {
        ## If delimiter is empty return false
        if( $param=='' ) { return false; }
        ## Set query delimiter
        $this->_query_param = filter_var($param, FILTER_SANITIZE_STRING);
    }
    
    ###################################
    ## Router->set_query_delimiter( $delimiter )
    ###################################    
    /**         -- Params --
     *  $delimiter : (string) delimiter - default is "/"
     *          -- Description --
     *  This method allows the setting of the query delimiter for creating query params.
     */
    public function set_query_delimiter( $delimiter='' )
    {
        ## If delimiter is empty return false
        if( $delimiter=='' ) { return false; }
        ## Set query delimiter
        $this->_query_delimiter = filter_var($delimiter, FILTER_SANITIZE_STRING);
    }
    
    ###################################
    ## Router->set_home_uri()
    ###################################    
    /**         -- Params --
     *  $uri : (string) name of home uri - default is "home"
     *          -- Description --
     *  This method allows the setting of the default home uri.
     */
    public function set_home_uri( $uri='' )
    {
        ## If uri is empty return false
        if( $uri=='' ) { return false; }
        ## Set home uri
        $this->_homeURI = filter_var($uri, FILTER_SANITIZE_STRING);
    }
    
    ###################################
    ## Router->set_namespace()
    ###################################    
    /**         -- Params --
     *  $namespace : (string) full namespace of classes.
     *          -- Description --
     *  This method allows the setting of a namespace for class file routes.
     */
    public function set_namespace( $namespace='' )
    {
        ## Set namespace
        $this->_namespace = filter_var($namespace, FILTER_SANITIZE_STRING);
    }    
    
    ###################################
    ## Router->add( $uri , $func )			 
    ###################################    
    /**         -- Params --
     *  $uri : (string) this is the path or route to add.
     *  $func: (string/function) this is either a function name or callback function 
     *  to call if route is reached.
     *          -- Description --
     *  This method adds routes and funcs to their appropriate arrays.
     */
    public function add( $uri='' , $func='' )
    {
        
        if( $uri=='' ) { return false; }
        ## Preped URI
        $preped_uri = $this->_prep_uri($uri);
        ## If uri path is document root set the uri to home page if not home page not already in array
        if( $uri=='/' ) { $this->_uri[] = $this->_prep_uri($this->_homeURI); }
        ## Otherwise put uri into array
        elseif( !in_array($preped_uri, $this->_uri) ) { $this->_uri[] = $preped_uri; }        
        ## If func is passed and not empty
        if( $func!='' ) :
            ## Add func to funcs array
            $this->_funcs[] = $func;
        endif;        
    }
    
    ###################################
    ## Router->route()
    ###################################    
    /**         -- Description --
     *  This method routes the current uri.
     */
    public function route()
    {
        ## Get current uri and if empty set to home
        $this->_getUri = ( $u = filter_input(INPUT_GET, $this->_query_param, FILTER_SANITIZE_STRING) ) ? $u : $this->_homeURI;
        ## Create URI array by exploding by forward slashes 
        $this->_getUriArray = array_filter( explode($this->_query_delimiter, $this->_getUri) );
        ## Preped current URI
        $preped_uri = $this->_prep_uri( $this->_getUriArray[0] );
        ## If preped URI not set in routes array return 404 error
        if( !in_array($preped_uri, $this->_uri) ) { $this->_show_404(); }
        ## Loop through added routes
        foreach( $this->_uri as $key => $value ) :            
            ## If current URI is in routes array
            if( $value==$preped_uri ) :
                ## Call function that will determine what how to proceed
                $this->_call_func($key);
            endif;              
        endforeach;        
    }    
    
    ###################################
    ## Router->_call_func( $key )
    ###################################    
    /**         -- Description --
     *  This method deciphers and calls the correct function.
     */    
    private function _call_func( $key )
    {
        ## Remove first query param from _uri array
        unset( $this->_getUriArray[0] );
        ## Re-index _uri array
        $query_params = array_values( $this->_getUriArray );
        ## Create class name
        $class_name = $this->_namespace.$this->_uri[$key];
        ## If a function was set and it is a callback function call it
        if( isset( $this->_funcs[$key] ) && is_callable( $this->_funcs[$key] )):
            ## Call calback function
            call_user_func($this->_funcs[$key], $query_params );
        ## If a function was set and is a name of a function and the function exists
        elseif( isset( $this->_funcs[$key] ) && is_string( $this->_funcs[$key] ) && function_exists( $this->_funcs[$key] ) ) :
            ## Call function
            $this->_funcs[$key]($query_params);        
        ## If class exists by the namespace and route name
        elseif( class_exists( $class_name ) ):    
            ## Instantiate class
            new $class_name($query_params);            
        endif;        
    }    
    
    ###################################
    ## Router->_prep_uri( $uri )
    ###################################    
    /**         -- Params --
     *  $uri : (string) is the uri parameter to clean and prepare.
     *          -- Description --
     *  This method converts to lowercase, removes slashes, and then capitalizes the first letter of a variable.
     */  
    private function _prep_uri( $uri )
    {
        ## Prepare and return the uri
        return ucfirst( trim( strtolower( $uri ) , '/' ) );
    }
    
    ###################################
    ## Router->_show_404()
    ###################################    
    /**         -- Description --
     *  This method shows a 404 message if no route is found. To set custom message/event
     *  just set the public $error_404;
     */  
    private function _show_404()
    {
        ## If function is a callback 
        if( is_callable($this->error_404) ) :
            ## Call the callback function
            call_user_func($this->error_404);
        ## If the function is a name
        elseif( function_exists($this->error_404) ):
            ## Call 404 function
            $this->error_404();
        else:
            ## Die and show 404 message.
            die('<h1>404 Error | Page Not Found!</h1>');
            exit();
        endif;
    }
   
}
