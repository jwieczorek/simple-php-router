# Simple-PHP-Router
A simple but powerful router for php with unlimited query parameters and multiple options for callback functions.

# First include the file

    // Bring the router into your project
    require_once('jwiezk-router.php');

# Example Usage
There are 4 ways to call user functions/methods for callback.

## Example 1

    // Create HomeClass
    class HomeClass
    {
    	function index($query_params)
    	{
    	    echo 'Doing home stuff!';
    	}
    }
    // Initialize router
    $router = new \JWIEZK\Router();
    // Set the home path with custom class
    $router->add('/',array(new HomeClass,'index'));
    // Call the route method
    $router->route();
    
## Example 2
    
    // Initialize router
    $router = new \JWIEZK\Router();
    // Set the home path with inline callback function
    $router->add('/',function($query_params){
        echo 'Doing home stuff!';
    });
    // Call the route method
    $router->route();

## Example 3
    // Home function
    function home_page($query_params) {
        echo 'Doing home stuff!';
    }
    
    // Initialize router
    $router = new \JWIEZK\Router();
    // Set the home path with function name as callback
    $router->add('/','home_page');
    // Call the route method
    $router->route();
    
## Example 4

In this example the router relies on having a class named "Home" accessible.

    // Create Home class
    class Home {
        function __construct($query_params) {
            echo 'Doing home stuff!';
        }
    }

    // Initialize router
    $router = new \JWIEZK\Router();
    // Set the home path
    $router->add('/');
    // Call the route method
    $router->route();
    
However, to change the default home class name, just do the following:

	// Initialize router
	$router = new \JWIEZK\Router();
	// Set default home route
	$router->set_home_uri('default');
	// Add home route
	$router->add('/');
	// Call the route method
    $router->route();

Also, if your route classes are in a namespace, just set the namespace:
	
	// Initialize router
	$router = new \JWIEZK\Router();
	// Set namespace
	$router->set_namespace('\routes\\'); // Don't forget to add the backslash to the end of the namespace.
	// Set default home route
	$router->set_home_uri('default');
	// Add home route
	$router->add('/');

## 404 Error Page

By default the 404 function does the following:

    die('<h1>404 Error | Page Not Found!</h1>');
    exit();

To customize the 404 error, a variable can be set to show a 404 page.

### Example 1

    // Set 404 error variable to a callback function
    $router->error_404 = function() {
        die('<h1>404 Error | Page Not Found!</h1>');
    };

### Example 2
    
    // 404 Error function
    function _404_page()
    {
        die('<h1>404 Error | Page Not Found!</h1>');
    }
    
    // Set router 404 error variable
    $router->error_404 = '_404_page';
    
## Query Parameters

By default the router uses the "/" (forward slash) to break the url. The "/" can be changed by doing the following:

    // Set delimiter to '-' dash/minus symbol
    $router->set_query_delimiter('-');
    
Once the url is parsed and borken into an array, just before the callback function is invoked, the "route" parameter in the array is removed. Then, the rest of the array is passed to route method/function.

### For example

In the url "http://www.example.com/user/account/jesse", ["user" is the method/function], and the rest are query parameters.

The router will look for a callback method/function called "user" and pass `array('account','jesse')` to it.

However, without a .htacces file, as explained below, your url will neen to be structured like: "http://www.example.com?uri=user/account/jesse".

## Changing Query Parameter

By default, as well as shown in the .htaccess example below, the query parameter used in the routing process is "uri". This can be easily changed by just changing the query parameter like this:

    // Set new query parameter
    $router->set_query_param('newQueryParam');
    
Please note, if you change the query parameter and are using an .htaccess file, you will need to change your .htaccess file as well.

#### Original .htaccess file:

`RewriteRule ^(.+)$ index.php?uri=$1 [QSA,L]`

#### Edited .htaccess file:

`RewriteRule ^(.+)$ index.php?newQueryParam=$1 [QSA,L]`

## .htaccess

In order to have clean urls, you need to add the following to, or create a .htaccess file with the following:

    <IfModule mod_rewrite.c>
    RewriteEngine On
    RewriteBase /
    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteRule ^(.+)$ index.php?uri=$1 [QSA,L]
    </IfModule>
    
Otherwise, you will have to add the query parameter "uri" to your url.
